#include <SPI.h>

const int nsp32SelectPin = 10;
SPISettings nsp32(5000000, MSBFIRST, SPI_MODE0); //5 MHz SPI clock

void setup() {
  pinMode(nsp32SelectPin, OUTPUT);
  SPI.begin();
  Serial.begin(9600);
  nsp32SensorID();
}

void loop() {
  if(Serial.available() > 0) {
    //Read string from serial port when available
    String cmd = Serial.readString();
    if(cmd == "REQ") {
      //Check for ID request command
      Serial.println(nsp32SensorID());  //Read NSP32 ID and print to serial port
    } else if(cmd == "VER") {
      while(Serial.available() == 0) {
        delay(10);
      }
      String nonce = Serial.readString();
      delay(50);
      Serial.println(nonce);
    }
  }
  delay(10);
}

String nsp32SensorID() {
  byte idRegisters[5];
  uint8_t otp = 0x0D;
  short lot_id, wafer_id, x_coord, y_coord;
  short sensor_type = 3;
  char lot_char;
  String id = "";
  
  for(int i = 0; i < 5; ++i) {
    SPI.beginTransaction(nsp32);
    digitalWrite(nsp32SelectPin, LOW);
    SPI.transfer(otp << 1);
    idRegisters[i] = SPI.transfer(0x00);
    digitalWrite(nsp32SelectPin, HIGH);
    SPI.endTransaction();
    ++otp;
  }

  lot_char = ((idRegisters[0] >> 3) & 0x1F) + 64;
  
  lot_id = 0;
  lot_id = ((idRegisters[0] << 3) & 0x38) | ((idRegisters[1] >> 5) & 0x05);
  lot_id = (lot_id << 8);
  lot_id |= ((idRegisters[1] << 3) & 0xF8) | ((idRegisters[2] >> 5) & 0x07);

  wafer_id = (idRegisters[2] & 0x1F);

  x_coord = (idRegisters[3] >> 1) & 0x7F;

  y_coord = ((idRegisters[3] << 6) & 0x40) | ((idRegisters[4] >> 2) & 0x3F);

  id += lot_char;
  id += lot_id;
  id += '-';
  id += wafer_id;
  id += '-';
  id += x_coord;
  id += '-';
  id += y_coord;
  id += '-';
  id += sensor_type;
  return id;
}

