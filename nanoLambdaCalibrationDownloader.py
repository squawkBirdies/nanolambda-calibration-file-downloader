#nanoLambda NSP32 Sensor Calibration Profile Downloader
#Utility that automatically dowloads the sensor profile file for a sensor given its serial number, also able to read out serial number directly from NSP32 using Arduino device
#Lawrence Fan for LESA ERC
#Copyright 2017
import sys
import os
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from tkinter import Radiobutton, Button, Label, Entry, Tk, W, IntVar, StringVar, Toplevel, Message, OptionMenu
import serial
import serial.tools.list_ports
import time
import random

def read(selected_port, group_1, group_2, group_3, group_4, group_5):
	#Description: This function is triggered by the 'Read' button in the GUI. Reads sensor serial ID from device and writes it to the GUI text boxes
	#Param: selected_port (name of serial port of device, string), group_1 (first part of ID, StringVar), group_2 (second part of ID, StringVar), group_3 (third part of ID, StringVar), group_4 (fourth part of ID, StringVar), group_5 (fifth part of ID, StringVar)
	#Return: None
	try:
		#Create serial socket 9600 baud 8N1 with 1 second timeout
		with serial.Serial(selected_port, 9600, timeout=1) as ser:
			ser.flushInput()
			ser.flushOutput()
			ser.write("REQ".encode())	#Send request to device
			count = 0 	#Wait time counter
			timeout = False
			while ser.in_waiting == 0:
				#Wait for results from device
				if count > 20:
					timeout = True
					break
				count += 1
				time.sleep(0.1)
			if timeout:
				#A couple of seconds have passed with no response from device
				#Stop and alert user
				top = Toplevel()
				top.title("Error")
				msg = Message(top, text="Serial communication failed!")
				msg.pack()
				button = Button(top, text='Dismiss', command=top.destroy)
				button.pack()
			else:
				#Read text string of result from serial socket
				ID = ser.readline()
				#Parse string by removing extraneous characters
				ID = str(ID).strip()
				ID = ID[2:]
				ID = ID[:-5]
				lot_type = ID[0]
				ID = ID[1:]
				#Split string into sensor ID components
				lot_number, wafer_number, chip_x, chip_y, res = ID.split('-')
				#Set text boxes to values received from device
				group_1.set(lot_number)
				group_2.set(wafer_number)
				group_3.set(chip_x)
				group_4.set(chip_y)
				group_5.set(res)
				#Alert user that sensor ID has been successfully received
				top = Toplevel()
				top.title("Success")
				msg = Message(top, text="Sensor ID read from device!")
				msg.pack()
				button = Button(top, text='Dismiss', command=top.destroy)
				button.pack()
			#Close serial socket once it is no longer needed
			ser.close()
	except:
		#Unable to open serial socket; alert user to problem
		print("Unexpected error:", sys.exc_info()[0])
		top = Toplevel()
		top.title("Error")
		msg = Message(top, text="Unable to open serial port!")
		msg.pack()
		button = Button(top, text='Dismiss', command=top.destroy)
		button.pack()

def enter():
	#Description: This function is triggered by the 'Enter' button. Attempts to download calibration file for sensor given its ID
	#Param: None
	#Return: None
	sensor_type_entered = False
	sensor = ""
	#Get type of sensor
	if sensor_type.get() == 1:
		sensor = "V1"
		sensor_type_entered = True
	elif sensor_type.get() == 2:
		sensor = "N1"
		sensor_type_entered = True
	elif sensor_type.get() == 3:
		sensor = "W1"
		sensor_type_entered = True
	else:
		#User did not enter the sensor type
		top = Toplevel()
		top.title("Error")
		msg = Message(top, text="Sensor type not entered!")
		msg.pack()
		button = Button(top, text='Dismiss', command=top.destroy)
		button.pack()
	if sensor_type_entered:
		#Check to ensure that sensor ID text strings are the correct length
		if (len(group_1.get()) <= 4) and (len(group_2.get()) <= 2) and (len(group_3.get()) <= 2) and (len(group_4.get()) <= 2) and (len(group_5.get()) <= 2):
			#Generate name of calibration file for this sensor
			serial_number = 'sensor_Z' + group_1.get() + "-" +  group_2.get() + '-' + group_3.get() + '-' + group_4.get() + '-' + group_5.get() + ".dat"
			folder_id = ""
			for file1 in file_list:
				if file1['title'] == sensor:
					#Find folder corresponding to the sensor type
					folder_id = file1['id']
					break

			if folder_id != "":
				#Folder opened successfully
				folder_list = drive.ListFile({'q': "'%s' in parents and trashed=false" % folder_id}).GetList()	#List files in the folder corresponding to the sensor type
				profile_id = ""
				for file2 in folder_list:
					#Iterate through files until file is found
					if file2['title'] == serial_number:
						profile_id = file2['id']
						break
				if profile_id != "":
					#If file is found, download file
					profile = drive.CreateFile({'id': profile_id})
					try:
						profile.GetContentFile(serial_number)	#Download file, alert user
						success_message = "Calibration file " + serial_number + " downloaded!" 
						top = Toplevel()
						top.title("Success")
						msg = Message(top, text=success_message)
						msg.pack()
						button = Button(top, text='Dismiss', command=top.destroy)
						button.pack()
					except:
						#Error if file is not downloaded
						top = Toplevel()
						top.title("Error")
						msg = Message(top, text="Calibration file not downloaded!")
						msg.pack()
						button = Button(top, text='Dismiss', command=top.destroy)
						button.pack()
				else:
					#Error if file is not found
					top = Toplevel()
					top.title("Error")
					msg = Message(top, text="Calibration file not found!")
					msg.pack()
					button = Button(top, text='Dismiss', command=top.destroy)
					button.pack()
			else:
				#Error if folder is not found
				top = Toplevel()
				top.title("Error")
				msg = Message(top, text="Sensor file folder not found!")
				msg.pack()
				button = Button(top, text='Dismiss', command=top.destroy)
				button.pack()
		else:
			#Serial number entered into application is invalid
			top = Toplevel()
			top.title("Error")
			msg = Message(top, text="Invalid serial number!")
			msg.pack()
			button = Button(top, text='Dismiss', command=top.destroy)
			button.pack()

#Get list of serial devices addressed by computer
#Linux/macOS: /dev/..., Windows: COMx
ports = serial.tools.list_ports.comports()
ports_list = []

for port in ports:
	#Create list of serial port names (string)
	ports_list.append(port.device)

path = os.path.expanduser('~')	#Get the path of the user's home directory
credLocation = path + "/Documents/nanoLambdaDownloader/"
os.chdir(credLocation)	#Switch working directory to this program's folder

#Google Account authentication
gauth = GoogleAuth()
gauth.LoadCredentialsFile("credentials.txt")	#Load user's credentials from file if it exists
if gauth.credentials is None:
	#Authenticate with Google if no credential file exists
	gauth.LocalWebserverAuth()
elif gauth.access_token_expired:
	#If OAuth2 token is expired, contact server to retrieve a new one
	gauth.Refresh()
else:
	gauth.Authorize()
gauth.SaveCredentialsFile("credentials.txt")	#Save credentials to text file

drive = GoogleDrive(gauth)

#ID of NanoLambda calibration profile folder
file_id = '0B451IY8adMirdFdRT3NicGRwUjQ'

try:
	#Get list of files on nanoLambda calibration profile folder
	file_list = drive.ListFile({'q': "'%s' in parents and trashed=false" % file_id}).GetList()
	#Create Tkinter GUI
	root = Tk()
	root.title("NSP32 Calibration File Downloader")
	root.configure()

	#Create Tkinter GUI elements and place them into a grid
	sensor_instructions = Label(root, text="Sensor Type: ").grid(row=0, column=0, sticky=W, columnspan=8)
	sensor_type = IntVar()
	Radiobutton(root, text="V1", variable=sensor_type, value=1).grid(row=1, column=0, sticky=W, columnspan=2)
	Radiobutton(root, text="N1", variable=sensor_type, value=2).grid(row=1, column=2, sticky=W, columnspan=2)
	Radiobutton(root, text="W1", variable=sensor_type, value=3).grid(row=1, column=4, sticky=W, columnspan=2)

	instructions = Label(root, text="Enter Serial Number: ").grid(row=2, column=0, sticky=W, columnspan=8)
	z = Label(root, text="Z", width=1)
	z.grid(row=3, column=0, sticky=W)
	group_1_txt = StringVar(root)
	group_1_txt.set("XXXX")
	group_1 = Entry(root, width=4, textvariable=group_1_txt)
	group_1.grid(row=3, column=1, sticky=W)
	dash_1 = Label(root, text="-").grid(row=3, column=2, sticky=W)

	group_2_txt = StringVar(root)
	group_2_txt.set("XX")
	group_2 = Entry(root, width=2, textvariable=group_2_txt)
	group_2.grid(row=3, column=3, sticky=W)
	dash_2 = Label(root, text="-").grid(row=3, column=4, sticky=W)

	group_3_txt = StringVar(root)
	group_3_txt.set("XX")
	group_3 = Entry(root, width=2, textvariable=group_3_txt)
	group_3.grid(row=3, column=5, sticky=W)
	dash_3 = Label(root, text="-").grid(row=3, column=6, sticky=W)

	group_4_txt = StringVar(root)
	group_4_txt.set("XX")
	group_4 = Entry(root, width=2, textvariable=group_4_txt)
	group_4.grid(row=3, column=7, sticky=W)
	dash_4 = Label(root, text="-").grid(row=3, column=8, sticky=W)

	group_5_txt = StringVar(root)
	group_5_txt.set("XX")
	group_5 = Entry(root, width=2, textvariable=group_5_txt)
	group_5.grid(row=3, column=9, sticky=W)

	#Create button to trigger calibration file download
	enter_button = Button(root, command = enter, text = "Enter").grid(row=3, column=10, sticky=W)

	serial_instructions = Label(root, text="Serial Port of NSP32 ID Reader: ").grid(row=4, column=0, sticky=W, columnspan=8)

	selected_port = StringVar(root)
	selected_port.set(ports_list[0])
	serial_menu = OptionMenu(root, selected_port, *ports_list).grid(row=5, column=0, sticky=W, columnspan=8)

	#Create button to trigger serial ID readout
	serial_button = Button(root, command =lambda: read(selected_port.get(), group_1_txt, group_2_txt, group_3_txt, group_4_txt, group_5_txt), text = "Read").grid(row=5, column=9, sticky=W)

	#Main loop of GUI
	root.mainloop()
except:
	print("Error: Unable to open Google Drive!")