# README #

### nanoLambda Calibration File Downloader ###

This is a Python program used to download calibration files for the NSP32 nano-spectrometer from nanoLambda's Google Drive folder. A simple Tkinter GUI is included to improve ease-of-use. Users enter the sensor type and its serial number, and the program downloads the calibration file. Alternatively, using an Arduino flashed with the included firmware, the program can automatically read out the serial number from an NSP32 sensor and fill it in. This program requires a directory 'nanoLambdaDownloader' in the user's 'Documents' folder. 

### NSP32 Downloader Device Pinout ###

This device is a Teensy 3.6 connected to a NSP32 breakout board designed to facilitate the process of downloading calibration files for large numbers of sensors.

(TO BE COMPLETED LATER)

### Resources ###
[LESA ERC](https://lesa.rpi.edu/)

[nanoLambda](https://nanolambda.myshopify.com/)

[PyDrive](https://pythonhosted.org/PyDrive/)

[PyInstaller:](https://github.com/pyinstaller/pyinstaller) Used to package Python scripts such as this one into executable files. 

[PySerial](https://pythonhosted.org/pyserial/)

[Python](https://www.python.org/downloads/)

[Teensy 3.6](https://www.pjrc.com/store/teensy36.html)

[Teensy Pinout Diagrams](https://www.pjrc.com/teensy/pinout.html)